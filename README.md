### ¿Cómo usar?

1. Clonar Proyecto
2. Crear entorno virtual `python3 -m venv .venv`
3. entrar al entrorno vitrual `source .venv/bin/activate`
4. Instalar dependencias `pip install -r requirements.txt`
5. Crear base de datos en local con nombre `revuelteria`
6. Ejecutar migraciones `python manage.py makemigrations` y luego `python manage.py migrate`
7. crear super ususario `python manage.py createsuperuser`
8. Iniciar servidor Django `python manage.py runserver`


#### Endpoints:
```
1. `http://localhost:8000` (Vista a travez de templates)
