from django.urls import path
from main.views import inicio, frutas, verduras, descuento, contacto, Login, logout, UsuarioCrear, carrito, carro,eliminar_item

app_name = "main"

urlpatterns = [
	path("",inicio, name="inicio"), 
	path("frutas/", frutas, name="frutas"),
	path("verduras/", verduras, name="verduras"),
	path("descuento/",descuento, name="descuento"),
	path("contacto/", contacto, name="contacto"),
	path("login/", Login.as_view(), name="login"),
	path("logout/", logout, name="logout"),
	path("registrarse/", UsuarioCrear.as_view(), name="registrarse"),
	path("carrito/", carrito, name="carrito"),
	path("carro/",carro, name="carro"),
	path("eliminar/item/<int:pk>", eliminar_item, name="eliminar_item"),
]
