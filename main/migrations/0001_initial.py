# Generated by Django 3.2.4 on 2021-06-27 14:47

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Frutas',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=75)),
                ('precio', models.IntegerField(default=0)),
                ('imagen', models.ImageField(null=True, upload_to='frutas/%Y/%m/%d/')),
            ],
        ),
        migrations.CreateModel(
            name='Verduras',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=75)),
                ('precio', models.IntegerField(default=0)),
                ('imagen', models.ImageField(upload_to='verduras/%Y/%m/%d/')),
            ],
        ),
        migrations.CreateModel(
            name='Carrito',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad', models.IntegerField(default=0)),
                ('fruta', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='main.frutas')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('verdura', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='main.verduras')),
            ],
        ),
    ]
