from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from main.models import Frutas, Verduras, Carrito
from django.views.generic.edit import FormView
from django.views.generic import CreateView
from django.contrib.auth.models import User
from main.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login
from django.contrib import auth

def inicio(request):
	if request.user.is_authenticated:
		return render(request, "index.html")
	else:
		return redirect("main:login")

def frutas(request):
	data = {}
	li = []
	for x in range(0,600):
		if x%3==0:
			li.append(x)
	objeto = Frutas.objects.all()
	data['object_list']=objeto
	data['li']=li
	return render(request, "fruteria.html", data)

def verduras(request):
	data = {}
	li = []
	for x in range(0,600):
		if x%3==0:
			li.append(x)
	objeto = Verduras.objects.all()
	data['object_list']=objeto
	data['li']=li
	return render(request, "verduras.html", data)

def descuento(request):
	return render(request, "descuento.html")

def contacto(request):
	return render(request, "contacto.html")

def carrito(request):
	producto = request.POST.get("producto")
	if producto == "fruta":
		cantidad = request.POST.get("cantidad")
		id_producto = request.POST.get("id_producto")
		objecto = Frutas.objects.get(pk=int(id_producto))
		instance = Carrito(usuario=request.user, fruta=objecto, cantidad=int(cantidad))
		instance.save()
		return redirect("main:frutas")
	elif producto == "verdura":
		cantidad = request.POST.get("cantidad")
		id_producto = request.POST.get("id_producto")
		objecto = Verduras.objects.get(pk=int(id_producto))
		instance = Carrito(usuario=request.user, verdura=objecto, cantidad=int(cantidad))
		instance.save()
		return redirect("main:verduras")

class Login(FormView):
	#Establecemos la plantilla a utilizar
	template_name = 'login.html'
	#Le indicamos que el formulario a utilizar es el formulario de autenticación de Django
	form_class = AuthenticationForm
	#Le decimos que cuando se haya completado exitosamente la operación nos redireccione a la url bienvenida de la aplicación personas
	success_url =  reverse_lazy("main:inicio")

	def form_valid(self, form):
		login(self.request, form.get_user())
		return super(Login, self).form_valid(form)

def logout(request):
    auth.logout(request)
    # Redirect to a success page.
    return redirect("main:login")

class UsuarioCrear(CreateView):
    model = User
    form_class = UserCreationForm
    template_name = "registrarse.html"
    success_url = reverse_lazy("main:registrarse")
# Create your views here.

def carro(request):
	context = {}
	data = Carrito.objects.filter(usuario=request.user)
	context['object_list']=data
	return render(request, "cart.html", context)

def eliminar_item(request, pk):
	objecto = Carrito.objects.get(pk=pk)
	objecto.delete()
	return redirect("main:carro")
