from django.db import models
from django.contrib.auth.models import User

class Frutas(models.Model):
	nombre = models.CharField(max_length = 75)
	precio = models.IntegerField(default = 0)
	imagen = models.ImageField(upload_to='frutas/%Y/%m/%d/', null=True)

	def __str__(self):
		return "Nombre: %s - Precio: %s"%(self.nombre, self.precio)

class Verduras(models.Model):
	nombre = models.CharField(max_length = 75)
	precio = models.IntegerField(default = 0)
	imagen = models.ImageField(upload_to='verduras/%Y/%m/%d/')

	def __str__(self):
		return "Nombre: %s - Precio: %s"%(self.nombre, self.precio)

class Carrito(models.Model):
	usuario = models.ForeignKey(User, on_delete=models.CASCADE)
	verdura = models.ForeignKey(Verduras, on_delete=models.CASCADE, null=True)
	fruta = models.ForeignKey(Frutas, on_delete=models.CASCADE, null=True)
	cantidad = models.IntegerField(default=0)

	def total_pagar(self):
		if self.verdura:
			return self.verdura.precio*self.cantidad
		if self.fruta:
			return self.fruta.precio*self.cantidad
		return 0
# Create your models here.


