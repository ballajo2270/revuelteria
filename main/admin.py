from django.contrib import admin
from main.models import Frutas, Verduras, Carrito

class FrutasAdmin(admin.ModelAdmin):
	list_display = ['nombre','precio','imagen']

class VerdurasAdmin(admin.ModelAdmin):
	list_display = ['nombre','precio','imagen']

class CarritoAdmin(admin.ModelAdmin):
	list_display = ['usuario', 'verdura', 'fruta', 'cantidad']

admin.site.register(Frutas, FrutasAdmin)
admin.site.register(Verduras, VerdurasAdmin)
admin.site.register(Carrito, CarritoAdmin)
# Register your models here.
